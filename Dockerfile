FROM docker.io/openjdk:11-jre
EXPOSE 8080:8080
RUN mkdir /app
COPY ./build/install/ktor-gradle/ /app/
WORKDIR /app/bin
CMD ["./ktor-gradle"]
