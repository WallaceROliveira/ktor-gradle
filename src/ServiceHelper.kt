package br.com.wall

import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.coroutines.CoroutineContext


object ServiceHelper {

    private val dispatcher: CoroutineContext

    init {
        dispatcher = newFixedThreadPoolContext(5, "database-pool")
    }


    suspend fun <T> dbQueryCourotine(block: () -> T): T = withContext(dispatcher) {
        TODO("Validar por que teste transação não pega dentro de dois courotines (CarServiceCarTest.`Busca todos os carros do banco`")
        transaction { block() }
    }

    //Skip thread pool...
    fun <T> dbQuery(block: () -> T): T = transaction { block() }
}

