package br.com.wall

import br.com.wall.carros.Carro
import br.com.wall.carros.CarrosService
import br.com.wall.carros.carros
import br.com.wall.config.api.TagApi
import br.com.wall.config.api.api
import br.com.wall.coroutinescope.CoroutineScopeService
import br.com.wall.coroutinescope.coroutineScope
import br.com.wall.module.mainModule
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.papsign.ktor.openapigen.OpenAPIGen
import com.papsign.ktor.openapigen.openAPIGen
import com.papsign.ktor.openapigen.route.apiRouting
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.*
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.response.*
import io.ktor.routing.Routing
import io.ktor.routing.get
import kotlinx.html.*
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database
import org.koin.ktor.ext.Koin

private const val OPEN_API_JSON_PATH = "/openapi.json"

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    initDB()
    install(Compression)
    install(DefaultHeaders)
    install(CallLogging)
    install(OpenAPIGen) {
        info {
            version = "1.0-SNAPSHOT"
            title = "Ktor Gradle"
            description =
                "Projeto em Kotlin com ktor, OpenApi(Swagger-UI), Postgre, Coroutine, Flyway"
        }

        server("http://localhost:8080/") {
            description = "Local server"
        }

        //rename DTOs from java type name to generator compatible form
//            replaceModule(DefaultSchemaNamer, object : SchemaNamer {
//                val regex = Regex("[A-Za-z0-9_.]+")
//
//                override fun get(type: KType): String {
//                    return type.toString().replace(regex) { it.value.split(".").last() }.replace(Regex(">|<|, "), "_")
//                }
//            })
    }
    install(ContentNegotiation) {
        jackson {
            registerModule(JodaModule())
            enable(SerializationFeature.INDENT_OUTPUT)
            disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            setSerializationInclusion(JsonInclude.Include.NON_NULL)
            configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        }
    }
    install(CORS) {
        anyHost()
        allowNonSimpleContentTypes = true
    }

    install(Koin) {
        modules(mainModule)
    }
    val client = HttpClient(CIO) {
        install(JsonFeature)
    }
    install(Routing) {
        // Get Default
        get("/") {
            call.respondHtml {
                body {
                    h1 {
                        +"Kotlin Ktor - Gradle"
                    }
                    p {
                        +"Acesse pelo link: "
                        a("http://localhost:8080/swagger-ui") { +"Swagger UI"}
                    }

                }
            }


        }
        // Arquivo Gerado
        get(OPEN_API_JSON_PATH) {
            call.respond(application.openAPIGen.api.serialize())
        }

        // Interface Swagger
        get("swagger-ui") {
            call.respondRedirect("/swagger-ui/index.html?url=$OPEN_API_JSON_PATH", true)
        }

        get("/testeRequest") {
            val response = client.get<List<Carro>>("http://localhost:8080/carros")
            call.respond(response)
        }

        /* Teste com Swagger-UI manual
         static("/static") {
            resources("files")
        }

        get("/swagger-ui") {
            call.respondRedirect("/swagger-ui/index.html?url=/static/test-swagger-manual.json", true)
        }
        */
    }
    apiRouting {
        api()
    }
}



fun initDB() {
    val config = HikariConfig("/hikari.properties")
    val ds = HikariDataSource(config)
    Database.connect(ds)
    Flyway.configure().dataSource("jdbc:postgresql://db:5432/kotlin_gradle", "wall", "wall").load().migrate()

    //Descomentar para rodar os inserts de teste
    CarsData.load()
}
