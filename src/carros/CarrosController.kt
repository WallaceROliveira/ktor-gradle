package br.com.wall.carros

import br.com.wall.config.api.TagApi
import com.papsign.ktor.openapigen.annotations.Path
import com.papsign.ktor.openapigen.annotations.parameters.PathParam
import com.papsign.ktor.openapigen.route.*
import com.papsign.ktor.openapigen.route.path.normal.*
import com.papsign.ktor.openapigen.route.response.respond
import io.ktor.features.*
import io.ktor.http.*

@Path("{id}")
data class IntParam(
    @PathParam("ID do Carro")
    val id: Int
)

@Path("{marca}")
data class StringBrandParam(
    @PathParam("Marca do Carro")
    val marca: String
)

@Path("{modelo}")
data class StringModelParam(
    @PathParam("Modelo do Carro")
    val modelo: String
)

fun NormalOpenAPIRoute.carros(carrosService: CarrosService) {
    tag(TagApi.Car) {
        route("/carros") {

            get<Unit, List<Carro>>(
                info("Busca todos os carros"),
                example = listOf(
                    Carro(1, "Honda", "Civic"),
                    Carro(1, "Honda", "CRV")
                )
            ) {
                respond(carrosService.getAllCars())
            }

            route("porMarca") {
                throws(
                    status = HttpStatusCode.NotFound.description("Erro ao buscar os carros por marca"),
                    exClass = BadRequestException::class
                ) {
                    get<StringBrandParam, List<Carro>>(info("Busca carro por marca")) { params ->
                        val cars = carrosService.getCarByBrand(params.marca)
                        respond(cars)
                    }
                }
            }

            route("porModelo") {
                throws(
                    status = HttpStatusCode.NotFound.description("Erro ao buscar os carros por modelo"),
                    exClass = BadRequestException::class
                ) {
                    get<StringModelParam, List<Carro>>(info("Busca carro por modelo")) { params ->
                        val cars = carrosService.getCarByModel(params.modelo)
                        respond(cars)
                    }
                }
            }

            throws(
                status = HttpStatusCode.NotFound.description("Carro não encontrado"),
                exClass = NotFoundException::class
            ) {
                get<IntParam, Carro>(
                    info("Busca carro por id"),
                    status(HttpStatusCode.OK),
                    example = Carro(1, "Honda", "Civic"),
                ) { params ->
                    val car = carrosService.getCar(params.id) ?: throw NotFoundException("Carro não encontrado")
                    respond(car)
                }
            }

            throws(
                status = HttpStatusCode.Conflict.description("Erro ao inserir carro"),
                exClass = BadRequestException::class
            ) {
                post<Unit, Carro, Carro>(
                    info("Cria um carro"),
                    status(HttpStatusCode.Created),
                    exampleRequest = Carro(marca = "Honda", modelo = "Civic"),
                    exampleResponse = Carro(1, "Honda", "Civic"),
                ) { _, body ->
                    val car = carrosService.addCar(body) ?: throw BadRequestException("Erro ao inserir carro")
                    respond(car)
                }
            }

            throws(
                status = HttpStatusCode.BadRequest.description("Erro ao atualizar carro"),
                exClass = BadRequestException::class
            ) {
                put<Unit, Carro, Carro>(
                    info("Atualiza um carro"),
                    status(HttpStatusCode.OK),
                    exampleRequest = Carro(1, "Honda", "Civic"),
                    exampleResponse = Carro(1, "Honda", "CRV"),
                ) { _, body ->
                    val car = carrosService.updateCar(body) ?: throw BadRequestException("Erro ao atualizar o carro")
                    respond(car)
                }
            }

            throws(
                status = HttpStatusCode.BadRequest.description("Erro ao excluir o carro"),
                exClass = BadRequestException::class
            ) {
                delete<IntParam, HttpStatusCode>(
                    info("Exclui carro por id"),
                    status(HttpStatusCode.OK)
                ) { params ->
                    if (carrosService.deleteCar(params.id)) respond(HttpStatusCode.OK)
                    else throw BadRequestException("Erro ao excluir o carro")
                }
            }
        }
    }
}

