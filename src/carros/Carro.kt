package br.com.wall.carros

data class Carro(
    val id: Int? = null,
    var marca: String,
    var modelo: String
)

// TODO Validar a utilização desse objeto futuramente
//@Response("Carro encontrado.", statusCode = 200)
//data class V1CarGetOkResponse(
//    override val data: Car
//) : OptionalResult<V1CarGetOkResponse.Car> {
//    companion object {
//
//        val EXAMPLE = V1CarGetOkResponse(
//            Car(
//                id = 1,
//                brand = "socks",
//                model = "teste"
//            )
//        )
//    }
//
//    data class Car(
//        val id: Int? = null,
//        var brand: String,
//        var model: String
//    )
//}