package br.com.wall.carros

import br.com.wall.ServiceHelper
import br.com.wall.`interface`.Tabela
import org.jetbrains.exposed.sql.*

object Carros : Tabela<Table>, Table() {
    val id = integer("id").autoIncrement().primaryKey()
    val marca = text("marca")
    val modelo = text("modelo")
    override fun get(): Table {
        return this
    }
}

class CarrosService {

    companion object {
        fun toCar(row: ResultRow): Carro = Carro(
        id = row[Carros.id],
        marca = row[Carros.marca],
        modelo = row[Carros.modelo]
        )

        fun toListCar(row: List<ResultRow>): List<Carro> {
            return row.map { toCar(it) }.toList()
        }
    }

    suspend fun getAllCars(): List<Carro> = ServiceHelper.dbQuery {
        Carros.selectAll().map { toCar(it) }
    }

    suspend fun getCar(id: Int?): Carro? = ServiceHelper.dbQuery {
        Carros.select {
            (Carros.id eq id!!)
        }.mapNotNull { toCar(it) }
            .singleOrNull()
    }

    suspend fun getCarByBrand(marca: String?): List<Carro> = ServiceHelper.dbQuery {
        Carros.select {
            (Carros.marca.upperCase() like "%${marca!!.toUpperCase()}%")
        }.map { toCar(it) }
    }

    suspend fun getCarByModel(modelo: String?): List<Carro> = ServiceHelper.dbQuery {
        Carros.select {
            (Carros.marca.upperCase() like "%${modelo!!.toUpperCase()}%")
        }.map { toCar(it) }
    }

    suspend fun addCar(carro: Carro): Carro? {
       val idCar = ServiceHelper.dbQuery {
           Carros.insert {
                it[marca] = carro.marca
                it[modelo] = carro.modelo
            } get Carros.id
        }
        return getCar(idCar)
    }

    suspend fun updateCar(carro: Carro): Carro? {
        val id = carro.id
        return if (id == null) {
            addCar(carro)
        } else {
            ServiceHelper.dbQuery {
                Carros.update({ Carros.id eq id }) {
                    it[marca] = carro.marca
                    it[modelo] = carro.modelo
                }
            }
            getCar(id)!!
        }
    }

    suspend fun deleteCar(id: Int?): Boolean = ServiceHelper.dbQuery {
        Carros.deleteWhere { Carros.id eq id!! } > 0
    }


}

