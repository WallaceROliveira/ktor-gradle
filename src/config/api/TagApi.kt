package br.com.wall.config.api

import com.papsign.ktor.openapigen.APITag

enum class TagApi(override val description: String) : APITag {
    Car("Version 1 API."),
    CoroutineScope("Version 1 API."),
}