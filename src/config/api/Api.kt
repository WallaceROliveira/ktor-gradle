package br.com.wall.config.api

import br.com.wall.carros.CarrosService
import br.com.wall.carros.carros
import br.com.wall.coroutinescope.CoroutineScopeService
import br.com.wall.coroutinescope.coroutineScope
import com.papsign.ktor.openapigen.route.path.normal.NormalOpenAPIRoute
import org.koin.java.KoinJavaComponent.inject

fun NormalOpenAPIRoute.api() {
    val carrosService: CarrosService by inject(CarrosService::class.java)
    val coroutineScopeService: CoroutineScopeService by inject(CoroutineScopeService::class.java)

    carros(carrosService)

    coroutineScope(coroutineScopeService)
}