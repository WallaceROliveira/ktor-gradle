package br.com.wall.module

import br.com.wall.carros.CarrosService
import br.com.wall.coroutinescope.CoroutineScopeService
import org.koin.dsl.module
import org.koin.dsl.single


val mainModule = module {
    single<CarrosService>()
    single<CoroutineScopeService>()
}