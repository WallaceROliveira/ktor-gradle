package br.com.wall.coroutinescope

import br.com.wall.config.api.TagApi
import com.papsign.ktor.openapigen.route.info
import com.papsign.ktor.openapigen.route.path.normal.NormalOpenAPIRoute
import com.papsign.ktor.openapigen.route.path.normal.get
import com.papsign.ktor.openapigen.route.response.respond
import com.papsign.ktor.openapigen.route.route
import com.papsign.ktor.openapigen.route.tag

fun NormalOpenAPIRoute.coroutineScope(coroutineScopeService: CoroutineScopeService) {

    tag(TagApi.CoroutineScope) {
        route("/coroutineScope") {
            get<Unit, Unit>(info("Teste do coroutine", "Rest para teste da chamada do courotine")) {
                respond(coroutineScopeService.getCoroutine())
            }
        }
    }

}
