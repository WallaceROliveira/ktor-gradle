package br.com.wall


import br.com.wall.carros.Carros
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

object CarsData {

    fun load() {
        transaction {
            if (Carros.selectAll().count() == 0) {
                Carros.deleteWhere { Carros.marca like "%%" }

                for (i in 1..100)
                    Carros.insert {
                        it[marca] = "Tesla-${i}"
                        it[modelo] = "Model #${i}"
                    }
            }
        }
    }
}
