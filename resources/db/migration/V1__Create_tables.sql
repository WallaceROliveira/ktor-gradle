create table if not exists carros(
    id serial primary key not null,
    marca varchar not null,
    modelo varchar not null
);
