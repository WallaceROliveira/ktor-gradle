package br.com.wall.dao

import br.com.wall.CarsData
import br.com.wall.carros.Carros
import org.jetbrains.exposed.sql.transactions.transaction

abstract class BaseDaoCarrosTest : BaseDaoTest() {

    fun criarTabelaEPopularDados() {
        transaction {
            super.criarTabela(Carros)
            CarsData.load()
        }
    }

   fun criarTabela() {
        super.criarTabela(Carros)
    }

    fun droparTabela() {
        super.droparTabela(Carros)
    }
}

