package br.com.wall.dao

import br.com.wall.carros.Carro
import br.com.wall.carros.Carros
import br.com.wall.carros.CarrosService.Companion.toCar
import br.com.wall.instrumentation.CarroModuleInstrumentation.getCar
import br.com.wall.instrumentation.CarroModuleInstrumentation.getCar2
import org.assertj.core.api.Assertions.assertThat
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CarrosDaoCarroTest : BaseDaoCarrosTest() {

    @Test
    fun `novo carro armazenado no banco`() {
        transaction {
            criarTabela()
            val carroValido = getCar()
            val carId = Carros.insert {
                it[marca] = carroValido.marca
                it[modelo] = carroValido.modelo
            } get Carros.id
            carId.let {
                val car = Carros.select {
                    (Carros.id eq it)
                }.mapNotNull { toCar(it) }.singleOrNull()

                car.apply {
                    assertThat(this).isEqualTo(
                        Carro(
                            carId,
                            carroValido.marca,
                            carroValido.modelo
                        )
                    )
                }
            } ?: throw IllegalStateException("Id do carro não pode ser nulo")
        }
    }

    @Test
    fun `carro sendo apagado no banco`() {
        transaction {
            criarTabela()
            val validPostCar = getCar()
            val carId = Carros.insert {
                it[marca] = validPostCar.marca
                it[modelo] = validPostCar.modelo
            } get Carros.id

            carId.let {
                val apagado = Carros.deleteWhere {
                    (Carros.id eq it)
                } > 0
                assertThat(apagado).isTrue
            } ?: throw IllegalStateException("Carro não deletado")
        }
    }


    @Test
    fun `carro sendo atualizado no banco`() {
        transaction {
            criarTabela()
            val validPostCar = getCar()
            val carId = Carros.insert {
                it[marca] = validPostCar.marca
                it[modelo] = validPostCar.modelo
            } get Carros.id

            val validNewCar = getCar2()
            carId.let {
                Carros.update {
                    it[id] = carId
                    it[marca] = validNewCar.marca
                    it[modelo] = validNewCar.modelo
                }

                val car = Carros.select {
                    (Carros.id eq it)
                }.mapNotNull { toCar(it) }
                    .singleOrNull()
                car.apply {
                    assertThat(this).isEqualTo(
                        Carro(
                            carId,
                            validNewCar.marca,
                            validNewCar.modelo
                        )
                    )
                }

            } ?: throw IllegalStateException("Carro não encontrado")
        }
    }
}