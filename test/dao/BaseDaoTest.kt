package br.com.wall.dao

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.BeforeEach

abstract class BaseDaoTest {

    @BeforeEach
    open fun setup() {
        var db = Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver", user = "root", password = "")
        db.useNestedTransactions = true
    }

    fun criarTabela(tabela: Table) {
        transaction {
            SchemaUtils.create(tabela)
        }
    }

    fun droparTabela(tabela: Table) {
        transaction {
            SchemaUtils.drop(tabela)
        }
    }
}

