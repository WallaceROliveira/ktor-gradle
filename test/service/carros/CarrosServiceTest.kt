package br.com.wall.service.car

import br.com.wall.carros.CarrosService
import br.com.wall.dao.BaseDaoCarrosTest
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.AssertionsForClassTypes.assertThat
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class CarrosServiceTest : BaseDaoCarrosTest() {

    private val mainThreadSurrogate = newSingleThreadContext("new thread")
    private val testDispatcher = TestCoroutineDispatcher()

    private val carrosService: CarrosService = CarrosService()

    @BeforeEach
    fun setUpCoroutine() {
        Dispatchers.setMain(testDispatcher)
    }

    @AfterEach
    fun tearDownCoroutine() {
        Dispatchers.resetMain()// resetando main
        mainThreadSurrogate.close() // fechando thread criada
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun exampleTestAsync(): Unit = runBlocking {
        val deferred = async {
            delay(1_000)
            withContext(Dispatchers.Default) {
                delay(1_000)
                "Teste de exemplo async"
            }
        }
        assertThat(deferred.await()).isEqualTo("Teste de exemplo async") // result available immediately
    }

    @Test
    fun `Busca todos os carros do banco`(): Unit = runBlocking {
        launch(Dispatchers.Main) {
            newSuspendedTransaction(Dispatchers.IO) {
                criarTabelaEPopularDados()
                val cars = carrosService.getAllCars()
                assertThat(cars.size).isEqualTo(100)
                droparTabela()
            }
        }
    }
}

