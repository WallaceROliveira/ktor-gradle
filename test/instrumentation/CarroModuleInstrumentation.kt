package br.com.wall.instrumentation

import br.com.wall.carros.Carro

object CarroModuleInstrumentation {
    fun getCar(): Carro {
        return Carro(
            marca = "brand",
            modelo = "model"
        )
    }

    fun getCar2(): Carro {
        return Carro(
            marca = "brand2",
            modelo = "model2"
        )
    }
}